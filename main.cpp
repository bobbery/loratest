// Für LoRa Node 151
// Verwende XDOT_L151CC als target
// und wende den patch LoRaNode151.patch auf das mbed-os-Verzeichnis an

#include <stdio.h>

#include "mbed.h"
#include "events/EventQueue.h"
#include "lorawan/LoRaRadio.h"
#include "SX1276_LoRaRadio.h"

using namespace events;

void OnTxDone( void );
void OnRxDone(const uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr);
void OnTxTimeout( void );
void OnRxTimeout( void );
void OnRxError( void );
void receive();

static EventQueue ev_queue(10 * EVENTS_EVENT_SIZE);
DigitalOut led(PB_8);

#define RF_FREQUENCY                                868000000 // Hz
#define TX_OUTPUT_POWER                             20        // 20 dBm
#define LORA_BANDWIDTH                              2         // [0: 125 kHz, 1: 250 kHz, 2: 500 kHz ]
#define LORA_SPREADING_FACTOR                       12         // [SF7..SF12]
#define LORA_CODINGRATE                             1         // [1: 4/5, 2: 4/6, 3: 4/7, 4: 4/8]
#define LORA_PREAMBLE_LENGTH                        8         // Same for Tx and Rx
#define LORA_SYMBOL_TIMEOUT                         5         // Symbols
#define LORA_FIX_LENGTH_PAYLOAD_ON                  false
#define LORA_FHSS_ENABLED                           false
#define LORA_NB_SYMB_HOP                            4
#define LORA_IQ_INVERSION_ON                        false
#define LORA_CRC_ENABLED                            true
#define RX_TIMEOUT_VALUE                            3500000   // in us

static radio_events_t RadioEvents;

SX1276_LoRaRadio radio(MBED_CONF_SX1276_LORA_DRIVER_SPI_MOSI,
                       MBED_CONF_SX1276_LORA_DRIVER_SPI_MISO,
                       MBED_CONF_SX1276_LORA_DRIVER_SPI_SCLK,
                       MBED_CONF_SX1276_LORA_DRIVER_SPI_CS,
                       MBED_CONF_SX1276_LORA_DRIVER_RESET,
                       MBED_CONF_SX1276_LORA_DRIVER_DIO0,
                       MBED_CONF_SX1276_LORA_DRIVER_DIO1,
                       MBED_CONF_SX1276_LORA_DRIVER_DIO2,
                       MBED_CONF_SX1276_LORA_DRIVER_DIO3,
                       MBED_CONF_SX1276_LORA_DRIVER_DIO4,
                       MBED_CONF_SX1276_LORA_DRIVER_DIO5,
                       MBED_CONF_SX1276_LORA_DRIVER_RF_SWITCH_CTL1,
                       MBED_CONF_SX1276_LORA_DRIVER_RF_SWITCH_CTL2,
                       MBED_CONF_SX1276_LORA_DRIVER_TXCTL,
                       MBED_CONF_SX1276_LORA_DRIVER_RXCTL,
                       MBED_CONF_SX1276_LORA_DRIVER_ANT_SWITCH,
                       MBED_CONF_SX1276_LORA_DRIVER_PWR_AMP_CTL,
                       MBED_CONF_SX1276_LORA_DRIVER_TCXO);

int main()
{
    bool isMaster = true;
    DigitalIn movement(PB_9);

    RadioEvents.tx_done = OnTxDone;
    RadioEvents.rx_done = OnRxDone;
    RadioEvents.rx_error = OnRxError;
    RadioEvents.tx_timeout = OnTxTimeout;
    RadioEvents.rx_timeout = OnRxTimeout;
    radio.init_radio( &RadioEvents );

    radio.set_channel( RF_FREQUENCY );

    radio.set_tx_config( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
                         LORA_SPREADING_FACTOR, LORA_CODINGRATE,
                         LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
                         LORA_CRC_ENABLED, LORA_FHSS_ENABLED, LORA_NB_SYMB_HOP,
                         LORA_IQ_INVERSION_ON, 2000000 );

    radio.set_rx_config( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
                         LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
                         LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON, 0,
                         LORA_CRC_ENABLED, LORA_FHSS_ENABLED, LORA_NB_SYMB_HOP,
                         LORA_IQ_INVERSION_ON, true );

    if (isMaster)
    {
        uint8_t StatusMsg[3];
        StatusMsg[0] = 0x55;
        StatusMsg[1] = 0xAA;
        while( 1 )
        {
            StatusMsg[2] = movement.read();
            led = !led;
            radio.send( StatusMsg, 3 );
            ThisThread::sleep_for(100ms);
        }
    }

    ev_queue.call(receive);
    ev_queue.dispatch_forever();
}

void receive()
{
    radio.receive( RX_TIMEOUT_VALUE );
}

void OnTxDone( void )
{
    radio.sleep();
}

void OnRxDone( const uint8_t */*payload*/, uint16_t /*size*/, int16_t /*rssi*/, int8_t /*snr*/)
{
    radio.sleep();
    led = !led;
    ev_queue.call(receive);
}

void OnTxTimeout( void )
{
    radio.sleep();
}

void OnRxTimeout( void )
{
    radio.sleep();
    ev_queue.call(receive);
}

void OnRxError( void )
{
    radio.sleep();
    ev_queue.call(receive);
}



